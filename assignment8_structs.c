#include<stdio.h>
#define MAX 100
struct student
{
	char name[MAX];
	char subject [MAX];
	int marks;
};

int main()
{
	int studentCount;
	printf("Input the number of students : ");
	scanf("%d",&studentCount);

	struct student details [studentCount];

	for(int i=0; i<=studentCount-1; i++)
	{
		printf("Enter the name :");
		scanf("%s",details[i].name);

		printf("Enter the subject :");
                scanf("%s",details[i].subject);
		
		printf("Enter marks :");
                scanf("%d",&details[i].marks);

		printf("---------------------------------\n");
	}

	printf("Name\tSubject\tMarks\n");
	for(int i=0; i<studentCount; i++)
        {
		printf("-----------------------------------------------------------------\n");
                printf("%s\t%s\t%d\n",details[i].name,details[i].subject,details[i].marks);
        }
	
	return 0;
}

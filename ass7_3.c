#include<stdio.h>

int main()
{
	int mat1row,mat2row,mat1col,mat2col;

	//getting row column count
	
	printf("Matrix 1 Details -> \n ");
	printf("\tRow count : ");
	scanf("%d",&mat1row);
	printf("\tColumn count : ");
	scanf("%d",&mat1col);

	printf("Matrix 2 Details -> \n ");
        printf("\tRow count : ");
        scanf("%d",&mat2row);
        printf("\tColumn count : ");
        scanf("%d",&mat2col);

        int matrix1 [mat1row][mat1col];
	int matrix2 [mat2row][mat2col];

	if(mat1row!=mat2row || mat1col!=mat2col || mat1row!=mat2col)
	{
		printf("Sorry! Input same row & column sizes for both.\n");
	}

	//getting data
	
	else
	{
		printf("Matrix 1 =>\n");
		for(int i=0; i<mat1row ; i++)
		{
			for(int j=0; j<mat1col ; j++)
			{
				printf("Enter value : ");
				scanf("%d", &matrix1[i][j]);
			}
		}

                
	        printf("\n\nMatrix 2 =>\n");
		for(int i=0; i<mat2row ; i++)
                {
                        for(int j=0; j<mat2col ; j++)
                        {
                                printf("Enter value : ");
                                scanf("%d", &matrix2[i][j]);
                        }
                }


		//addition
	
		int addition[mat1row][mat1col];
	
		for(int i=0; i<mat1row; i++)
		{
			for(int j=0; j<mat1col; j++)
			{
				addition[i][j]=matrix1[i][j]+matrix2[i][j];
			}
		}


		printf("Addition => \n");
		for(int i=0; i<mat1row; i++)
                {
                        for(int j=0; j<mat1col; j++)
                        {
                                printf("%d\t ",addition[i][j]);
                        }
			printf("\n");
                }

		//multiplication
		
		int multiplication[mat1row][mat1col];

		for(int i=0; i<mat1row; i++)
		{
			for(int j=0; j<mat1col; j++)
			{
				multiplication[i][j]=0;
                              
                                for(int k=0; k<mat1col; k++)
				{
					multiplication[i][j]+=matrix1[i][k]*matrix2[k][j];	
				}
			}
		}
		
		printf("\n\nMultiplication => \n");
                for(int i=0; i<mat1row; i++)
                {
                        for(int j=0; j<mat1col; j++)
                        {
                                printf("%d\t ",multiplication[i][j]);
                        }
                        printf("\n");
                }
	}
	return 0;
}

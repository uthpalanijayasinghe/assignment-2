#include<stdio.h>

//defining constants
#define fixedcost 500
#define attencost 3

//functions
double profit(double);
int attendees(double);
double income(double);
double cost(double);


int main()
{
	for(double price=5; price<=100; price+=5)
	{
		//Printing the profit with price
		printf("The ticket price = Rs. %.2f\t|   The profit : Rs. %.2f\n",price,profit(price));
		printf("-------------------------------------------------------------------\n");
	}
}

double profit(double price)
{
	double profit= income(price)-cost(price);
	
}

int attendees(double price)
{
	return 120-(((price-15)/5)*20);
}

double income(double price)
{
	return attendees(price)*price;
}

double cost(double price)
{
	return fixedcost+(attendees(price)*attencost);
}

#include <stdio.h>
#include <string.h>

int main()
{
        //taking values from the user
        char sentence[255];
        printf("Enter a sentence : ");
        scanf("%[^\n]%*c",sentence);

        //reversing the sentence
        for(int i=strlen(sentence); i>=0; i--)
        {
                printf("%c",sentence[i]);
        }

        printf("\n");
}

